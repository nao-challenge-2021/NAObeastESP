#pragma once

#include <Arduino.h>

typedef std::function<void()> OnClickListener;

class ClickButton {
  byte buttonPin;        // the number of the pushbutton pin
  int buttonState = LOW; // variable for reading the pushbutton status
  unsigned long lastDebouceTime = 0UL;
  int debounceTime = 50;
  OnClickListener onClickListener = nullptr;

  void onClickHappened() {
    if (onClickListener != nullptr) {
      onClickListener();
    }
  }

public:
  ClickButton(byte buttonPin) {
    this->buttonPin = buttonPin;
    pinMode(buttonPin, INPUT);
  }

  void setDebounceTime(int value) {
    debounceTime = value;
  }

  void setOnClickListener(OnClickListener listener) {
    onClickListener =  listener;
  }
  
  void update() {
    if(millis() - lastDebouceTime > debounceTime) {
      lastDebouceTime = millis();
      int currentButtonState = digitalRead(buttonPin);
      if((buttonState != currentButtonState) && (currentButtonState == LOW)) {
        onClickHappened();
      }
      // read the state of the pushbutton value:
      buttonState = digitalRead(buttonPin);
    }
  }
};
