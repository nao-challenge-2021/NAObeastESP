/*******************************************************************************
 * This sketch auto configures its WiFi settings. If no known network was found
 * within 30 seconds at startup of the board, a new "esp8266ap" network will be
 * created with default password "12345678".
 * A captive portal will be available on this temporary Wifi-AP network to allow 
 * the configuration of a new nearby WiFi network. From within the captive
 * portal tap on the hamburger icon and select "Configure new AP" to setup the
 * credentials of the network. Once the operation is completed the new IP adress
 * will be displayed both on the capitve portal and on the serial port. Select
 * "reset" from the menu to restart the device directly in client mode.
 ******************************************************************************/
#include <ESP8266WiFi.h>
#include <AutoConnect.h>
#include "ClickButton.h"

const uint8_t BUTTON_PIN = LED_BUILTIN;
const char * NAO_IP = "192.168.181.201";
const uint16_t NAO_PORT = 8080;
const char * KEYWORD = "NAOremote";
const int BLINK_MILLIS = 100;

AutoConnect captiveWiFiConfigPortal;
ClickButton clickButton{BUTTON_PIN};

void sendNaoKeyword(const char * keyword) {
  // Connect to the NAO via a TCP socket.
  WiFiClient client;
  client.setTimeout(1000);
  Serial.print("Connecting to ");
  Serial.print(NAO_IP);
  Serial.print(':');
  Serial.println(NAO_PORT);
  if(!client.connect(NAO_IP, NAO_PORT)) {
    blinkLed(700);
    Serial.println("Connection refused");
    return;
  }
  if(client.connected()) {
    // Send the magic keyword.
    Serial.print("Sending ");
    Serial.println(keyword);
    client.println(keyword);
    client.stop();
  }
}

void onButtonClicked() {
  sendNaoKeyword(KEYWORD);
}

void blinkLed(int durationMillis) {
  for(int ms = 0; ms<durationMillis; ms=ms+BLINK_MILLIS) {
    digitalWrite(LED_BUILTIN, LOW);
    pinMode(LED_BUILTIN, OUTPUT);
    delay(BLINK_MILLIS/2);
    pinMode(LED_BUILTIN, INPUT);
    digitalWrite(LED_BUILTIN, HIGH);
    delay(BLINK_MILLIS/2);
  }
}

void setup() {
  blinkLed(1000);
  Serial.begin(115200);
  Serial.println("Starting WiFi AP configuration.");
  if(captiveWiFiConfigPortal.begin()) {
    Serial.print("WiFi connected. IP = ");
    Serial.println(WiFi.localIP());
    blinkLed(500);
    clickButton.setOnClickListener(onButtonClicked);
  } else {
    Serial.println("WiFi AP configuration failed.");
  }
}

void loop() {
  captiveWiFiConfigPortal.handleClient();
  clickButton.update();
}
