# NAObeastESP

This project uses an Arduino Wemos D1 Mini board and a push button connected to pin D4 to message a magic word back to the socket server hosted on the robot NAO.
The NAO v6 server Choregraphe project can be found in the **extras** folder. 

Required Arduino Core: https://github.com/esp8266/Arduino

See also: https://www.wemos.cc/en/latest/d1/d1_mini.html

The project must be imported first as an **Sketchbook** in Arduino IDE. Go to menu **File -> Preferences** and point **Sketchbook location:** to this folder.

The main button is connected to **Pin D4** and must connect to ground when pushed (equals to a bridge between D4 and **Pin G**).

The board to be selected in Arduino IDE menu is: **Tools -> Board -> ESP8266 Boards -> LOLIN(WEMOS) D1 R2 & mini**.

The serial port over the USB cable can be found in the menu: **Tools -> Port**.

Please checkout the documentation of the **[AutoConnect](https://github.com/Hieromon/AutoConnect/blob/master/README.md)** project for how to connect the Arduino to a new WiFi network. 
